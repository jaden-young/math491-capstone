\documentclass[11pt]{amsart}

\usepackage{amsfonts,amssymb,amscd,amsmath,mathrsfs,amsthm}


% Uncomment the following line in order to include graphics

\usepackage[pdftex]{graphicx}

\usepackage{subcaption}

\usepackage{youngtab}

\usepackage{multicol}
%
%  The following are margin definitions used in the publication at the end of term.
%

\setlength{\oddsidemargin}{0.25in}  %please do not change
\setlength{\evensidemargin}{0.25in} %please do not change
\setlength{\marginparwidth}{0in} %please do not change
\setlength{\marginparsep}{0in} %please do not change
\setlength{\marginparpush}{0in} %please do not change
\setlength{\topmargin}{0in} %please do not change

\setlength{\footskip}{.3in} %please do not change
\setlength{\textheight}{8.75in} %please do not change
\setlength{\textwidth}{6in} %please do not change
\setlength{\parskip}{4pt} %please do not change

\theoremstyle{plain}
\newtheorem{thm}{Theorem}[section]
\newtheorem{proposition}[thm]{Proposition}

\theoremstyle{remark}
\newtheorem*{remark}{Remark}

\theoremstyle{definition}
\newtheorem{defn}[thm]{Definition}
\newtheorem{exmp}[thm]{Example}


\newcommand{\upgeqlower}{\mathbin{\text{\rotatebox[origin=c]{-90}{$\geq$}}}}


\author{Jaden Young}
\title{Dynamics of Plane Partitions}

\thanks{Advisors: Dr. Jessica Striker and Corey Vorland}
\email{jaden.young@ndsu.edu}
\address{Department of Mathematics, North Dakota State University, PO Box 6050, Fargo, ND 58108-6050, USA}

\newcommand{\PP}{\mathcal{P}}

\begin{document}
\begin{abstract}
  We present a general algorithm for the computation of plane partitions of any
given symmetry class. We reduce the problem to the search for a fundamental
domain. Given a fundamental domain of a symmetry class and the operation that
determines the symmetry class, we leverage bijections between plane partitions,
Young tableaux, and certain subsets of partially ordered sets to arrive at an
already solved problem.
\end{abstract}
\maketitle

\section{Introduction}\label{sec:introduction}
Plane partitions function exceedingly well as visualization tools given their
many bijections with other combinatorial objects. While constructing a single
plane partition from another object is rather straightforward, the process of
computing plane partitions, especially of particular symmetry classes, isn't as
obvious. We explore various bijections between plane partitions and other
objects to give an algorithm for efficiently computing every plane partition of
a given symmetry class. \cite{Bressoud99}
% This paper explores the
% structure and behavior of combinatorial operations on partitions and plane
% partitions. The operations of rowmotion and promotion\cite{Striker17} have been
% studied on order ideals in posets, giving relations to various other
% mathematical objects\cite{StWi12}. One of our aims is to extend these relations
% to partitions and plane partitions. In addition to actions, we study the
% structure of these objects, particularly the structure of their orbits. These
% orbit structures can exhibit various properties, such as
% resonance\cite{DiPeSt17} and homomesy\cite{Vorland17}.

\section{Preliminaries}
\label{sec:preliminaries}
\subsection{Partitions}
\label{sec:partition} We begin with a simple concept to serve as a foundation we
can later build on. A \emph{partition}, denoted $\lambda$ of a positive integer
$n$, is a way of writing $n$ as the sum of positive integers. For example, the
number $7$ can be written as $3 + 2 + 1 + 1$. To give a visual representation of
this partition, a Young diagram (in English notation) $\lambda =
{\tiny\yng(3,2,1,1)}$ is used. In English notation, the number of squares in
each row is nonincreasing (equivalently, weakly decreasing) from top to bottom.
If we write $\lambda_i$ to be the number of boxes in row $i$, starting from the
top row, then $\lambda_i \geq \lambda_{i+1}$. We say that $\lambda$ has a
\textit{shape} of $(\lambda_1, \lambda_2, \dots, \lambda_{k})$, where $k
\leq n$, and $\sum_{i=1}^k \lambda_i = n$. Then the partition $3 + 2 + 1 + 1$ has shape $(3, 2, 1, 1)$. This
restriction on shape turns out to be useful when we naturally begin filling in
these empty squares, as the shape of the diagram itself can determine various
properties.

\subsection{Young Tableaux}
\label{sec:young-tableau} In general, a \emph{Young tableau} is a filling of a
\emph{Young diagram} with arbitrary mathematical objects. We will be considering
especially \emph{decreasing} tableaux.
\begin{defn}
  \label{def:young-tableau} Let $\lambda$ be a Young diagram of an integer
partition. Define a \emph{decreasing Young tableau} of shape $\lambda$ with
maximum entry $q$ to be a filling of $\lambda$ with positive integers such that
entries weakly decrease from top to bottom along columns and from left to right
along rows. More concretely, if we use conventional matrix indexing and let
$a_{i,j}$ denote the entry in the $i^{th}$
row and $j^{th}$ column, then
  \begin{align*} a_{i,j} &\geq a_{i+1,j} \\ a_{i,j} &\geq a_{i,j+1}
  \end{align*}
\end{defn}
For example, take $\lambda = \yng(2,2)$, a partition of the integer
$4$. A particular decreasing Young tableau $T$ of shape $\lambda$ is shown in
Figure \ref{fig:young-tableau}.
\begin{figure}
  $T = \young(31,21)$
  \caption{A decreasing Young tableau of shape $(2,2)$}
  \label{fig:young-tableau}
\end{figure}
To see that $T$ is a valid decreasing Young tableau, notice that all of the
inequalities in
\renewcommand{\arraystretch}{0.8}
\begin{tabular}{l@{}c@{}r}
  3             & $\geq$ & 1 \\
  $\upgeqlower$ &        & $\upgeqlower$ \\
  2             & $\geq$ & 1 \\
\end{tabular}
hold. We will use $T$ as a running example through the rest of this paper.
These numbers $3, 2, 1, 1$ should seem familiar, since we also used them in the
previous section as an example of a partition of $7$. Indeed, just as Young
diagrams are equivalent to integer partitions, decreasing Young tableaux are
equivalent to \emph{plane partitions}, one of our core objects of study.

\subsection{Plane Partitions}
\label{sec:plane-partition}
\begin{defn}\label{def:plane-partition}
  A \emph{plane partition}, written $\pi$, of an integer $n$, is a decreasing
Young tableau whose entries sum to $n$.
\end{defn}

  To give a geometric interpretation of plane partitions in three dimensions, we
can visualize a plane partition $\pi$ of $n$ as a stack of $n$ many unit cubes pushed
up against the corner of a room. Place the top left corner of the tableau
representation of $\pi$ at the corner of the room, and stack $\pi_{i,j}$ many
cubes on each tile at position $i,j$ of $\pi$. Figure \ref{fig:with-box} shows
our running example $T$ in this way. Since the entries of $\pi$ are
nonincreasing by definition, this stacking is stable. That is, if we imagine a
gravitational force pulling toward that corner of the room, the blocks do not
move. If we let that room be the positive orthant and that corner be the
origin, then we can naturally identify each cube with its Cartesian coordinates
from $\mathbb{N}^3$.

\begin{defn}\label{def:plane-partition-diagram}
  \cite{Okada89} A diagram of a plane partition $\pi$, written $D(\pi)$, is the set
$$
  D(\pi) = \{(i,j,k) \in \mathbb{N}^3: a_{i,j} \in \pi \text{ and } 1 \leq k \leq a_{i,j}\}.
$$
\end{defn}


%   Consider a plane
% partition $\pi$ of an integer $n$ inside of an $a \times b \times c$ box
% (denoted $\mathcal{B}(a,b,c)$) to be a stacking of $n$ many unit cubes in the
% rectangular solid $[0,a] \times [0,b] \times [0,c]$ that is stable under
% gravitational attraction towards the origin \cite{Kup95}. This means that there
% are no holes in the stacking, that the stacking is flush against the back walls
% of the box, and that the number of cubes in each stack weakly decreases moving
% away from the origin.
%   Our running example $T$ is then a plane partition of $7$, since $\sum_{i,j}
% n_{i,j} = 3 + 2 + 1 + 1 = 7$. While this definition can be convenient for
% algebraic manipulation, the true study of plane partitions begins when we take a
% more geometric view. An alternate definition is more commonly used when working
% plane partitions, and will be necessary for our later discussion of
% \emph{symmetry classes}.

% Figure \ref{fig:with-box} gives an example of our familiar
% $T$ represented in this way.

% %There are a number of ways to define a \emph{plane partition}. One of the most
% %natural is to say that a plane partition is a stacking of unit cubes inside of
% a %box of size $a \times b \times c$ (written $\mathcal{B}(a,b,c)$), with all of
% %the cubes pushed towards the back corner. The number of cubes in each stack
% %must be nonincreasing moving away from the origin; such that if we imagine %a
% gravitational force pulling from the origin, the stacking would remain stable.
% %$a, b, c$ correspond to the traditional $x, y, z$ axes, respectively. Figure
% %\ref{fig:with-box} shows a plane partition in $\mathcal{B}(2,2,3)$. Notice
% %that the cubes are flush against the back walls.

\begin{figure}
  \includegraphics[width=0.3\textwidth]{img/with-box}
  \caption[plane partition]{Plane partition representation of Figure \ref{fig:young-tableau}}
  \label{fig:with-box}
\end{figure}

\subsection{Symmetry Classes}
\label{sec:symmetries} Now that we have some definitions of objects, it is
natural to look for familiar properties they may exhibit. Symmetry is one of the
most natural properties when working with objects that have as nice of a
geometric interpretation as plane partitions. Three natural symmetry operations
have been identified on plane partitions \cite{Kup95}:
\begin{enumerate}
\item \label{def:tau} \emph{Transposition}, $\tau$, is a transposition of $\pi$
across its main diagonal. Equivalently, $\tau$ is a reflection through the plane
$x = y$ on $D(\pi)$, given by $\tau(i, j, k) = (j, i, k)$. Plane partitions
invariant under $\tau$ are called \textbf{symmetric plane partitions} (SPP).
% of the box enclosing the plane partition. Conventionally, the plane under
% discussion is the plane $x = y$, and plane partitions that are symmetric about
% this plane are called \textbf{symmetric plane partitions} (SPP).
% Notice that this is a three dimensional analog of the underlying Young tableau
% being transposition symmetric, i.e. symmetric about its main diagonal.
\item \label{def:rho}\emph{Rotation}, $\rho$, is defined most naturally on
$D(\pi)$. Algebraically it is a cyclic permutation of the axes: $\rho(i,j,k) =
(k, i, j)$. Thinking more geometrically, $\rho$ is a rotation of $120^\circ$
about the long diagonal of the box $\mathcal{B}(a,b,c)$ enclosing the plane
partition (the line intersecting the origin and the point $(a,b,c)$). Figure
\ref{fig:rotation} gives an example of this interpretation. Plane partitions
invariant under $\rho$ are called \textbf{cyclically symmetric plane partitions}
(CSPP).
\item \emph{Complementation}, $\kappa$, again is much more intuitive when
  defined on $D(\pi)$. $\kappa$ can be thought of as filling every space
in a box enclosing a plane partition with cubes, and removing the original plane
partition. A plane partition $\pi$ that is equal (after reorientation) to its own
complement $\kappa(\pi)$ is called a \textbf{self complementary plane partition}
(SCPP).
\end{enumerate}

\begin{figure}
  \centering
  \begin{subfigure}{.19\linewidth}
    \includegraphics[width=\textwidth]{img/anim/rotation/cspp/cspp-0.pdf}
  \end{subfigure}
  \begin{subfigure}{.19\linewidth}
    \includegraphics[width=\textwidth]{img/anim/rotation/cspp/cspp-2.pdf}
  \end{subfigure}
  \begin{subfigure}{.19\linewidth}
    \includegraphics[width=\textwidth]{img/anim/rotation/cspp/cspp-4.pdf}
  \end{subfigure}
  \begin{subfigure}{.19\linewidth}
    \includegraphics[width=\textwidth]{img/anim/rotation/cspp/cspp-6.pdf}
  \end{subfigure}
  \begin{subfigure}{.19\linewidth}
    \includegraphics[width=\textwidth]{img/anim/rotation/cspp/cspp-0.pdf}
  \end{subfigure}
  \caption{Rotation of a CSPP in $\mathcal{B}(4,4,4)$}
  \label{fig:rotation}
\end{figure}

  The study of the first two of these operations, $\tau$ and $\rho$, applied to
plane partitions began with P.A. MacMahon at the turn of the 20th century during
the course of his work with symmetric functions. Indeed, CSPPs and SPPs are two
of the six symmetry classes of plane partitions that are in correspondence with
the conjugacy classes of $S_3$ acting on $\mathbb{N}_3$, and hence on $D(\pi)$ \cite{Kup95}.
In 1986, Richard Stanley introduced the action of complementation, giving rise
to four additional symmetry classes by combining $\kappa$ with other classes
\cite{Stanley86}. Because of this historical divide, \textbf{totally symmetric
plane partitions} (TSPP) are unintuitively those that are (transpose) symmetric
and cyclically symmetric, but not self complementary. Adding symmetry under
$\kappa$ to TSPPs yields \textbf{totally symmetric self complementary plane
partitions} (TSSCPP).

  Figure \ref{fig:pp-sym-examples} gives a few different examples of plane
partitions inside of $\mathcal{B}(4,4,4)$ with varying symmetries. Notice the
differences between Figures \ref{fig:spp}, \ref{fig:cspp}, and \ref{fig:tspp},
as SPPs, CSPPs, and TSPPs will be of particular interest in this paper.

\begin{figure} \centering
  \begin{subfigure}[a]{0.3\textwidth}
\includegraphics[width=\textwidth]{img/spp}
    \caption{SPP}
    \label{fig:spp}
  \end{subfigure}\hfill
  \begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{img/cspp}
    \caption{CSPP}
    \label{fig:cspp}
  \end{subfigure}
  \begin{subfigure}[c]{0.3\textwidth}
\includegraphics[width=\textwidth]{img/tspp}
    \caption{TSPP}
    \label{fig:tspp}
  \end{subfigure}\hfill
  \begin{subfigure}[d]{0.3\textwidth}
\includegraphics[width=\textwidth]{img/tsscpp}
    \caption{TSSCPP}
    \label{fig:tsscpp}
  \end{subfigure}\hfill
  \begin{subfigure}[e]{0.3\textwidth}
\includegraphics[width=\textwidth]{img/pp-no-symmetry}
    \caption{No symmetry}
    \label{fig:pp-no-symmetry}
  \end{subfigure}
  \caption{Plane partition examples in $\mathcal{B}(4,4,4)$}
  \label{fig:pp-sym-examples}
\end{figure}


% We look again to very closely related objects with their own natural operations.
% Intuitively, if we shrink each cube in a plane partition down to a point, and
% connect those points based on how their corresponding cubes were stacked, we end
% up creating an \emph{order ideal} of a \emph{partially ordered set}.

\subsection{Posets}
\begin{defn}
  \label{def:poset} A \textbf{partially ordered set}, or \textbf{poset} $\PP$ is
a set $P$ together with a binary relation ``$\leq$'', denoted $\PP = (P, \leq)$.
$\leq$ is a \emph{partial order}, meaning that for all $a, b, c \in P$, the
relation $\leq$ must be:
  \begin{enumerate}
    \item Reflexive: $a \leq a$
    \item Antisymmetric: If $a \leq b$ and $b \leq a$ then $a = b$
    \item Transitive: If $a \leq b$ and $b \leq c$ then $a \leq c$
  \end{enumerate}
\end{defn}

% Having provided a definition, we will now in the true spirit of mathematics
  % begin to abuse the notation, and refer to $\PP$ and $P$ interchangeably.
  We will only be considering finite posets. Note that since $\PP$ is a
\emph{partially} ordered set, it is possible for two elements of the poset to be
completely incomparable, meaning that neither $a \leq b$ nor $b \leq a$. If $a
\leq b$ and $a \neq b$, we write $a < b$. This ``partial'' notion contrasts with
a \emph{total ordering}, such as the natural $\leq$ on the set of integers,
where each integer is indeed comparable to every other. We say that for $a, b, x
\in \PP$ that $b$ \emph{covers} $a$ if $a < b$ and there exists no $x \in \PP$
such that $a < x < b$.

  We use a Hasse diagram to graphically represent a poset $\PP$. We draw each
element of $P$ as a vertex in the plane and draw a line segment upward
from $x$ to $y$ if $y$ covers $x$. While vertical position in the diagram is
normally sufficient to determine order for ``two dimensional'' diagrams, we use
directed line segments for clarity.

  There are some notable subsets of a poset $\PP$, examples of which are given
in Figure \ref{fig:posets} as Hasse diagrams. A \emph{chain} $C$ is a subset of
distinct elements $c_i \in P$ such that $c_1 < c_2 < \ldots < c_n$. Equivalently,
$C$ is totally ordered. An \emph{antichain} $A$ is a subset of $P$ such that
each pair of elements in $A$ are incomparable. A type of subset we will take
particular interest in is an \textit{order ideal}.

\begin{figure}
  \centering
  \begin{subfigure}{.3\linewidth}
    \centering
    \includegraphics[width=.8\textwidth]{img/ipe/poset.pdf}
    \caption{A poset}
    \label{fig:poset}
  \end{subfigure}
  \begin{subfigure}{.3\linewidth}
    \centering
    \includegraphics[width=.8\textwidth]{img/ipe/cover-relation.pdf}
    \caption{$a$ covers $b$}
    \label{fig:cover-relation}
  \end{subfigure}\\
  \begin{subfigure}{.3\linewidth}
    \centering
    \includegraphics[width=.8\textwidth]{img/ipe/chain.pdf}
    \caption{Chain}
    \label{fig:chain}
  \end{subfigure}
  \begin{subfigure}{.3\linewidth}
    \centering
    \includegraphics[width=.8\textwidth]{img/ipe/antichain.pdf}
    \caption{Antichain}
    \label{fig:antichain}
  \end{subfigure}
  \begin{subfigure}{.3\linewidth}
    \centering
    \includegraphics[width=.8\textwidth]{img/ipe/order-ideal.pdf}
    \caption{Order Ideal}
    \label{fig:order-ideal}
  \end{subfigure}
  \caption{Hasse diagrams showing poset definitions}
  \label{fig:posets}
\end{figure}

\begin{defn}
\label{def:oideal} A subset $I$ of $P$ is called an \emph{order ideal} if for
any $t \in I$, if $s \leq t$ in $P$ then $s \in I$. The set of all order ideals
of $P$ is denoted $J(P)$.
\end{defn}

  We say that an order ideal $I$ is \emph{generated} by its maximal elements.
That is, if we know each $e \in I$ such that $e \nleq f$ for all $f \in I$, then
we have enough information to find every other element of $I$. Notice that for
any order ideal $I \subseteq P$, its maximal elements form an antichain. Indeed,
the antichains of any poset $P$ are in canonical bijection with the order ideals
of $P$, by mapping each order ideal to the antichain formed by its maximal
elements. The correspondence of antichains and order ideals can be seen in
Figures \ref{fig:antichain} and \ref{fig:order-ideal}. We then say that every
antichain uniquely generates an order ideal.

  An intuitive example of a poset is the set of all humans partially ordered by
the binary relation ``is a descendant of''. Clearly $child < father <
grandmother$, forming a chain. But under this relation siblings are incomparable
since neither $brother$ is a descendant of $sister$ nor the converse, so the set
$\{brother, sister\}$ is an antichain. Then the set of all descendants of a
single person is an order ideal generated by that person.

  Notice that chains are themselves posets, and we now introduce the notion of a
\emph{product of chains}. Let $(A, \leq_A)$, $(B, \leq_B)$ be chain posets. Then
their product is $(A \times B, \leq_{A \times B})$, where $(a,b) \leq_{A \times
B} (a', b')$ if and only if $a \leq_A a'$ and $b \leq_B b'$. Figure
\ref{fig:product-of-chains} shows a product of two chains, each of length three.
When discussing the product of three chains, we write elements in the form
$(a,b,c)$ and consider the \textit{product order}, or coordinatewise order, such
that $(a,b,c) \leq (a',b',c')$ if and only if
$a \leq a', b \leq b', \text{ and } c \leq c'$.

\begin{figure}
  \centering
  \includegraphics[width=.7\textwidth]{img/ipe/checkerboard}
  \caption{Product of two chains, each of length 3}
  \label{fig:product-of-chains}
\end{figure}

  We now have the means to exhibit the close relationship between plane
partitions and posets. A plane partition $P$ in a box
$\mathcal{B}(a,b,c)$ is exactly an order ideal $I$ in the product of three
chains of length $a, b, c$ poset. Figure \ref{fig:with-box-as-oi} shows our
running example from Figure \ref{fig:with-box} as an order ideal in a product of
three chains of length 2, 2, and 3 poset.


\begin{figure}
  \centering
  \includegraphics[width=.7\textwidth]{img/ipe/with-box-as-oi}
  \caption{Figure \ref{fig:with-box} as an order ideal in a product of chains
  poset.}
  \label{fig:with-box-as-oi}
\end{figure}

% Just as transitioning from Young tableaux to plane partitions gave us operations
% more natural to plane partitions to work with, so too do we get new operations
% when moving to posets. Two closely related such operations are \emph{rowmotion},
% Row, and \emph{promotion}, Pro.

% \begin{defn}
%   \label{def:rowmotion} Let $I$ be an order ideal of a poset $P$. Rowmotion,
% Row: $J(P) \rightarrow J(P)$ on $I$ is the order ideal generated by the minimal
% elements of $P \setminus I$.
% \end{defn}

%   While this definition is satisfyingly simple, it does leave a bit to be desired
% in terms of machinery. For that, we reach for group theory. Let us define the
% toggle


\section{Computing Plane Partitions}
  Plane partitions serve exceedingly well as visualization tools. Their rich set
of bijections with other mathematical objects allows us to greatly simplify the
process of drawing graphical representations of those other objects. There are
well defined and intuitive rules for graphing a plane partition; stacking
blocks on top of each other just as in the real world. Symmetries become easily
visible. On the other hand, finding an intuitive and visually informative way to
represent other objects isn't normally quite as easy.

We are then tempted to ask ourselves for a drawing of \textit{every} plane
partition, or more interestingly, every plane partition of any particular
symmetry class. While this would be computationally infeasible for a human to
do, we can go reasonably far with the aid of computers. SageMath \cite{sagemath}
is a free and open source mathematics software used in this paper to explore the
dynamics of plane partitions and posets.

To give an example of the graphical advantages of plane partitions, we can
consider Hasse diagrams. It is oftentimes difficult to draw a ``good'' Hasse
diagram of a poset. In fact, there are infinitely many ways of drawing a Hasse
diagram for any given poset. The simple approach of starting with minimal
elements and incrementally adding greater elements most often results in
``poor'' Hasse diagrams, in that internal structure and symmetries become lost.
For example, Figure \ref{fig:hasse-is-hard} shows what Sage draws for the CSPP
in Figure \ref{fig:cspp} as an order ideal in the product of 3 chains lengths
$4\times 4\times 4$ poset.

\begin{figure}
  \centering
  \includegraphics[width=.5\textwidth]{img/cspp-as-oi-sage}
  \caption{Sage drawing of the CSPP from Figure \ref{fig:cspp} as an order ideal.}
  \label{fig:hasse-is-hard}
\end{figure}

\subsection{Previous Work}
SageMath 8.0.0, the current stable version as of the writing of this paper, has
the ability to iterate over all of the plane partitions that fit inside of a
given box $\mathcal{B}(a,b,c)$. To do this, SageMath utilizes the bijection
between plane partitions and \textit{semistandard Young tableau}, a tableau in
which positive integer entries weakly increase along rows and strictly increase
along columns. This approach, while convenient, does have limitations.

Currently, in order to generate plane partitions of a specific symmetry class
given a box size, Sage must first compute every plane partition of appropriate
size and check each individually for symmetry. This process results in a large
waste of computation, and limits inquiry into any particular symmetry class.
While bijections between other classes of tableaux do exist, we search instead
for a more general solution.

\subsection{Fundamental Domain}
We borrow from the field of Topology the notion of a \emph{fundamental domain}.
If we take any topological space $\chi$ and a group that acts on it, the images
of a single point under the group action form an orbit. A fundamental domain $D$
is a subset of $\chi$ that contains only one point from each of these orbits.

Intuitively, a fundamental domain is the smallest possible subset of a set with
some symmetry operation from which the original set is recoverable by applying
that operation to the fundamental domain. To give an example that is not
entirely accurate, but does illustrate the concept well, consider a symmetric
$n\times n$ matrix $A$. Either the lower or upper triangular regions of $A$
form a fundamental domain under the operation of transposition.

A corollary to the definition of a fundamental domain is that applying a
symmetry operation to a fundamental domain to recover the full object
necessarily produces an object invariant under that symmetry operation.
The fundamental domain plays a key role in our efforts to iterate plane
partitions of any particular symmetry class. By considering only the fundamental
domain, we avoid unnecessary computation.

We use the bijections between plane partitions, order ideals, and antichains to
give a general algorithm for computing plane partitions of any particular
symmetry class. In particular, the problem of iterating the antichains of a
poset has already been solved directly in Sage, and is where our work will end.
The idea for the algorithm is as follows:

Consider a symmetry class of plane partitions restricted to a box
$\mathcal{B}(a,b,c)$ determined by a symmetry operation $f$. Let $D$ be the
fundamental domain of the symmetry class as the diagram of a plane partition.
Map $D$ to its corresponding order ideal $I$ in the product of chains of lengths
$a, b, c$ poset with the usual product order. Notice that $I$ is itself a poset, and
now the order ideals of $I$ are in direct correspondence with the plane
partitions of the original symmetry class. As order ideals of a poset are in
canonical bijection with antichains of that poset, we then consider the set
$A(I)$, the antichains of $I$. Calculating $A(I)$ has a known solution that we
now defer to.

We then recover the plane partitions of the symmetry class exactly from the
antichains of the fundamental domain poset. Map $A(I)$ to $J(I)$ with their
canonical bijection. Then map the order ideals to corresponding (partial) plane
partitions and apply $f$ to the partial plane partitions to recover the
full objects.

In effect, we have reduced the problem of devising an algorithm or finding a
bijection per symmetry class down to determining the fundamental domain. Our
approach also applies to the general case of all plane partitions. The
fundamental domain of all posets inside of a box $\mathcal{B}(a,b,c)$ is simply
the product of chains of lengths a, b, c poset, with the symmetry operation $f$
being the identity function. In the following sections, we illustrate this
approach to computing specifically SPPs, CSPPs, and TSPPs.

\subsection{Symmetric Plane Partitions}
As defined previously in Section \ref{sec:symmetries}, $\tau$ acts as
transposition on a plane partition $\pi$. The intuitive candidates for the
fundamental domain of symmetric plane partitions are then the lower and upper
triangular regions of $\pi$. When viewed algebraically, since $\tau$ sends
$(i,j,k)$ to $(j,i,k)$, it makes sense that we require only one of the first two
coordinates to recover the full plane partition. Figure
\ref{fig:spp-fundamental-domain} shows the choice of the lower triangular region
as a poset in $\mathcal{B}(3,3,3)$. The particular fundamental domain is given
by the set
$$
D = \{(x,y,z) \in \mathbb{N}^3: x \geq y, \text{ and } x, z < 3\}
$$
together with the usual product order. We then apply our algorithm and recover
the full symmetric plane partitions by applying $\tau$ to the resulting partial
plane partitions.
\begin{figure}[htb!]
  \centering
  \includegraphics[width=.8\textwidth]{img/ipe/spp-fundamental-domain}
  \caption[SPP Fundamental Domain]{Fundamental domain of SPPs in
    $\mathcal{B}(3,3,3)$}
  \label{fig:spp-fundamental-domain}
\end{figure}

\subsection{Cyclically Symmetric Plane Partitions}
CSPPs are determined by $\rho$, a rotation of $120^{\circ}$ about the long
diagonal of the box enclosing a plane partition. Since a rotation of $360^{\circ}$
results in the identity function, it follows that a fundamental domain will
contain $\frac{120}{360} = \frac{1}{3}$ of the points of the full product of
three chains poset. Figure \ref{fig:cspp-fundamental-domain} gives an example of
a choice of fundamental domain in $\mathcal{B}(3,3,3)$ given by
$$
D = \{(x,y,z) \in \mathbb{N}^3: x, y \geq z \text{ and } z < 3\}
$$
together with the usual product order. We then apply our algorithm and recover
the full plane partitions by applying $\rho$ twice to the resulting partial
plane partitions.

\begin{figure}[htb!]
  \centering
  \includegraphics[width=.8\textwidth]{img/ipe/cspp-fundamental-domain}
  \caption[SPP Fundamental Domain]{Fundamental domain of CSPPs in
    $\mathcal{B}(3,3,3)$}
  \label{fig:cspp-fundamental-domain}
\end{figure}
\subsection{Totally Symmetric Plane Partitions}
Totally symmetric plane partitions are those plane partitions that are both
invariant under $\tau$ and $\rho$. It follows then that the fundamental domain
of TSPPs is exactly the intersection of the fundamental domains of SPPs and
CSPPs. Figure \ref{fig:tspp-fundamental-domain} gives an example of a choice of
fundamental domain in $\mathcal{B}(3,3,3)$ given by
$$
D = \{(x,y,z) \in \mathbb{N}^3: x \geq y \geq z \text{ and } z < 3\}
$$
together with the usual product order. To recover the full TSPPs, we can either
apply $\tau \circ \rho \circ \rho$ or equivalently $\rho \circ \rho \circ \tau$.

\begin{figure}[htb!]
  \centering
  \includegraphics[width=.8\textwidth]{img/ipe/tspp-fundamental-domain}
  \caption[SPP Fundamental Domain]{Fundamental domain of TSPPs in
    $\mathcal{B}(3,3,3)$}
  \label{fig:tspp-fundamental-domain}
\end{figure}

\bibliographystyle{plain} \bibliography{bib}{}
\end{document}