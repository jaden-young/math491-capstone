import numpy

from sage.combinat.plane_partition import PlanePartition
from sage.combinat.posets.posets import Poset
from sage.combinat.posets.poset_examples import Posets
from sage.sets.set import Set
from sage.calculus.var import var
from sage.misc.functional import symbolic_prod as product
from sage.misc.all import prod


def product_order(a, b):
    return all(x <= y for x, y in zip(a, b))


def squarify(arr, val):
    """Make a numpy array `arr` square by padding with `val`"""
    (a, b) = arr.shape
    if a > b:
        padding = ((0, 0), (0, a - b))
    else:
        padding = ((0, b - a), (0, 0))
    return numpy.pad(arr, padding, mode='constant', constant_values=val)


def symmetrize_array(arr):
    """
    Symmetrize a numpy matrix `arr` about its main diagonal.

    `arr` can be either upper or lower triangular.
    """
    square = squarify(arr, 0)
    return numpy.maximum(square, square.T)


def symmetrize_tableau(tab):
    """
    Symmetrize a tableau (as a list of lists) about its main diagonal.

    The tableau need not be square, and can be upper or lower triangular.
    """
    M = numpy.matrix(tab)
    sym = symmetrize_array(M)
    return sym.tolist()


def chain_poset_coord_to_xyz(c):
    return (c[0][0], c[1], c[0][1])


def xyz_to_chain_poset_coord(c):
    return ((c[0], c[2]), c[1])


def chain_poset_oi_to_xyz_oi(oi):
    r"""
    Takes an iterable of points of the form ((x,z),y) and returns
    a Set of the same points, formatted as (x, y, z)
    """
    return Set([chain_poset_coord_to_xyz(c) for c in oi])


def xyz_oi_to_chain_poset_oi(oi):
    r"""
    Takes an iterable of points of the form (x, y, z) and returns
    a Set of the same points, formatted as ((x,z), y)
    """
    return Set([xyz_to_chain_poset_coord(c) for c in oi])


def oi_to_array(oi):
    from collections import defaultdict
    d = defaultdict(int)
    max_x = 0
    max_y = 0
    for x, y, z in oi:
        if x > max_x:
            max_x = x
        if y > max_y:
            max_y = y
        if z + 1 > d[(x, y)]:
            d[(x, y)] = z + 1
    arr = numpy.zeros((max_x + 1, max_y + 1), dtype=numpy.int)
    for (x, y), z in d.iteritems():
        arr[x, y] = z
    return arr


def oi_to_tableau(oi):
    """
    Construct a tableau representation of an order ideal.

    oi is an iterable of points (x,y,z) that form an order ideal.

    Returns a tableau as a list of lists T, where T[i][j] is the
    highest value of z with x = i and y = j.
    """
    arr = oi_to_array(oi)
    return arr.tolist()


def oi_to_symmetric_plane_partition(oi):
    arr = oi_to_array(oi)
    arr = symmetrize_array(arr)

    return PlanePartition(arr)


def oi_to_plane_partition(oi, symmeterize=False):
    r"""
    Returns the PlanePartition representation of oi.

    oi is an iterable of 3D coordinates of the form (x, y, z)
    symmeterize specifies whether or not to symmeterize the
    coordinates over the x=y plane.

    """
    tab = oi_to_tableau(oi)
    return PlanePartition(tab)


def chain_poset_oi_to_plane_partition(oi):
    r"""
    Takes on order ideal from a product of chains poset as an iterable
    of points of the form ((x,z), y) and returns the PlanePartition formed
    by those points
    """
    i = chain_poset_oi_to_xyz_oi(oi)
    return oi_to_plane_partition(i)


def plane_partition_to_oi(pp):
    r"""
    Returns the order ideal representation of a PlanePartition as a Set
    of coordinates of the form (x, y, z)
    """
    # return pp.cells()
    tab = pp.to_tableau()
    # # [[2 1],[1]]
    return Set((x, y, z)
               for x, row in enumerate(tab) for y, zval in enumerate(row)
               for z in range(0, zval))


def plane_partition_to_chain_poset_oi(pp):
    r"""
    Returns the order ideal representation of a PlanePartition as a
    Set of coordinates of the form ((x,z), y)
    """
    oi = plane_partition_to_oi(pp)
    return xyz_oi_to_chain_poset_oi(oi)


def product_of_3_chains_poset(a, b, c):
    a_chain = Posets.ChainPoset(a)
    b_chain = Posets.ChainPoset(b)
    c_chain = Posets.ChainPoset(c)
    return a_chain.product(b_chain).product(c_chain)


def pp_smallest_box(pp):
    """Find the smallest (x,y,z) box pp could fit inside of"""
    x = len(pp)
    y = len(pp[0])
    z = pp[0][0]
    return (x, y, z)


def box_poset(x, y, z):
    r"""
    Construct a Poset comprised of every point x,y,z inside a box size x,y,z

    Partial order is defined naturally, such that (x,y,z) <= (r,s,t) iff
    x <= r and y <= s and z <= t.
    """

    elems = [(i, j, k) for i in range(x) for j in range(y) for k in range(z)]
    return Poset((elems, product_order))


def poset_order_ideals_iter(pset):
    return (Set(pset.order_ideal(antichain))
            for antichain in pset.antichains_iterator())


def plane_partition_rowmotion(pp, box=None):
    r"""Perform rowmotion on the order ideal representation of a PlanePartition.

    INPUT:

    - ``pp`` -- PlanePartition; the object on which to perform rowmotion

    - ``box`` -- 3 tuple; The (x,y,z) box bounding ``pp``. If ``None``, the
      box is calculated to be the smallest possible box boudning ``pp``.
      Rowmotion is performed in the product of chains length x, y, and z
      poset.

    OUTPUT: The image of the order ideal representation of ``pp`` in the product
    of chains poset corresponding to ``box`` under rowmotion as a PlanePartition.

    EXAMPLES:

    Running rowmotion on a PlanePartition

        sage: pp = PlanePartition([[2, 1], [1]])
        sage: ppPrime = plane_partition_rowmotion(pp); ppPrime
        Plane partition [[2, 2], [2, 1]]

    """
    if box is None:
        box = pp_smallest_box(pp)
    pset = product_of_3_chains_poset(*box)
    oi = plane_partition_to_chain_poset_oi(pp)
    image = pset.rowmotion(oi)
    return chain_poset_oi_to_plane_partition(image)


def plane_partition_rowmotion_orbit_iter(pp, box=None, stop=True, check=True):
    r"""
    Iterate over the rowmotion orbit of the order ideal representation of ``pp``
    in the product of chains poset bounded by ``box``.

    This function looks at PlanePartitions bounded by a box of size X x Y x Z as
    order ideals in the product of three chains lengths X, Y, Z poset. It then
    performs rowmotion on those order ideal representations and reinterperets the
    the images as a PlanePartitions.

    INPUT:

    - ``pp`` -- PlanePartition; the object whose orbit under rowmotion will
      be iterated

    - ``box`` -- 3 tuple; The (x,y,z) box bounding ``pp``. If ``None``, the
      box is calculated to be the smallest possible box boudning ``pp``.
      Rowmotion is performed in the product of chains length x, y, and z
      poset.

    - ``stop`` -- Boolean (default: True); If ``True``, stop iteration after
      completing one cycle of rowmotion. If ``False``, continue indefinitely.

    - ``check`` --Boolean (default: True); If ``True``, ``pp`` is checked for
    being a valid order ideal in the product of chains poset formed by ``box``.

    OUTPUT:

    - an iterator over the orbit of rowmotion applied to ``pp`` thought of as
    an order ideal in the product of chains poset formed by ``box``. This
    iterator ``I`` has the property that ``I[0] == pp`` and that each ``i``
    satisfies ``plane_partition_rowmotion(I[i]) == I[i+1]``, where ``I[i+1]``
    has to be understood as ``I[0]`` if it is undefined (out of range).

    EXAMPLES:
        sage: pp = PlanePartition([[2,1],[1]])
        sage: list(plane_partition_rowmotion_orbit_iter(pp))
        [Plane partition [[2, 1], [1, 0]], Plane partition [[2, 2], [2, 1]], Plane partition [[2, 2], [2, 2]], Plane partition [], Plane partition [[1]]]
    """
    if box is None:
        box = pp_smallest_box(pp)
    pset = product_of_3_chains_poset(*box)
    oideal = plane_partition_to_chain_poset_oi(pp)
    itr = pset.rowmotion_orbit_iter(oideal, stop=stop, check=check)
    return (chain_poset_oi_to_plane_partition(oi) for oi in itr)


def spp_fundamental_domain(box):
    if len(box) < 3:
        raise ValueError("invalid box size")
    x, y, z = box
    r = min(x, y)

    elems = [(i, j, k) for i in range(r) for j in range(r) for k in range(z)
             if i >= j]
    return Poset((elems, product_order))


def spp_iter(box):
    pset = spp_fundamental_domain(box)
    return (oi_to_symmetric_plane_partition(oi)
            for oi in poset_order_ideals_iter(pset))


# def prod(factors):
#     from operator import mul
#     return reduce(mul, factors, 1)


def spp_cardinality(box):
    i, j = var('i,j')
    x, y, s = box
    r = min(x, y)
    # p1_factors = [(2 * i + s - 1) / (2 * i - 1) for i in range(1,r + 1)]
    p1 = product((2 * i + s - 1) / (2 * i - 1), i, 1, r)
    p2_factors = [((i + j + s - 1) / (i + j - 1))
                  for j in range(1, r + 1) for i in range(1, j)]
    # p1 = prod(p1_factors)
    p2 = prod(p2_factors)
    return p1 * p2


def spp_rowmotion_orbit_iter(pp, stop=True, check=True):
    box = pp_smallest_box(pp)
    pset = spp_fundamental_domain(box)
    oideal = plane_partition_to_oi(pp)
    itr = pset.rowmotion_orbit_iter(oideal, stop=stop, check=True)
    return (oi_to_symmetric_plane_partition(oi) for oi in itr)


def plot_OI(myposet, OI):
    colors = {}
    colors['red'] = OI
    p = myposet.plot(vertex_colors=colors, figsize=10)
    return p


def spp_rowmotion_orbits(box):
    pset = spp_fundamental_domain(box)
    return ([oi_to_symmetric_plane_partition(oi) for oi in orb]
            for orb in pset.rowmotion_orbits())


def orbit_structure(listoforbits):
    p = [len(orb) for orb in listoforbits]
    p.sort()
    return p


def tspp_fundamental_domain(box):
    if len(box) != 3:
        raise ValueError("invalid box size")
    x, y, z = box
    r = min(x, y, z)

    elems = [(i, j, k) for i in range(r) for j in range(r) for k in range(r)
             if i >= j and j >= k]
    return Poset((elems, product_order))


def tspp_cardinality(box):
    i, j = var('i,j')
    r = min(box)
    factors = [(i + j + r - 1) / (i + (2 * j) - 2)
               for j in range(1, r + 1) for i in range(1, j + 1)]
    return prod(factors)


def tspp_iter(box):
    pset = tspp_fundamental_domain(box)
    return (oi_to_plane_partition(oi) for oi in poset_order_ideals_iter(pset))


def tspp_rowmotion_orbits(box):
    pset = tspp_fundamental_domain(box)
    return (oi_to_symmetric_plane_partition(oi)
            for orb in pset.rowmotion_orbits() for oi in orb)


def cspp_fundamental_domain(box):
    if len(box) != 3:
        raise ValueError("invalid box size")
    r = min(box)

    elems = [(i, j, k) for i in range(r) for j in range(r) for k in range(r)
             if i <= k and j <= k]
    return Poset((elems, product_order))


def cspp_cardinality(box):
    if len(box) != 3:
        raise ValueError("invalid box size")
    r = min(box)
    h, j = var('h,j')
    p1 = product((r + h + j - 1) / (2 * h + j - 1), j, h, r)
    p2 = product(((3 * h - 1) / (3 * h - 2)) * p1, h, 1, r)
    return p2


def cspp_iter(box):
    pset = cspp_fundamental_domain(box)
    return (oi_to_tableau(oi) for oi in poset_order_ideals_iter(pset))


# def oi_to_cspp(oi):
#     arr = oi_to_array(oi)
