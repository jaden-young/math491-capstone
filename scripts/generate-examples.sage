#!/usr/bin/env sage
# yapf: disable

# This script generates various plots to be displayed in the paper.
# To run:
#     sage generate-examples.sage

from sage.combinat.plane_partition import *


def chain_poset_coord_to_xyz(c):
    return (c[0][0], c[1], c[0][1])


def xyz_to_chain_poset_coord(c):
    return ((c[0], c[2]), c[1])


def chain_poset_oi_to_xyz_oi(oi):
    r"""
    Takes an iterable of points of the form ((x,z),y) and returns
    a Set of the same points, formatted as (x, y, z)
    """
    return Set([chain_poset_coord_to_xyz(c) for c in oi])


def xyz_oi_to_chain_poset_oi(oi):
    r"""
    Takes an iterable of points of the form (x, y, z) and returns
    a Set of the same points, formatted as ((x,z), y)
    """
    return Set([xyz_to_chain_poset_coord(c) for c in oi])


def oi_to_tab(oi):
    """
    Construct a tableau representation of an order ideal.

    oi is an iterable of points (x,y,z) that form an order ideal.

    Returns a tableau as a list of lists T, where T[i][j] is the
    highest value of z with x = i and y = j.
    """
    from collections import defaultdict
    d = defaultdict(int)
    for x, y, z in oi:
        if z + 1 > d[(x,y)]:
            d[(x,y)] = z + 1
    mat = Matrix(d)
    return [row.list() for row in mat]


def symmeterize_LT_tab(tab):
    for i, row in enumerate(tab):
        for j in range(i+1, len(row)):
            tab[i][j] = tab[j][i]
    return tab


def oi_to_symmetric_plane_partition(oi):
    tab = oi_to_tab(oi)
    tab = symmeterize_tab(tab)
    return PlanePartition(tab)


def oi_to_plane_partition(oi, symmeterize=False):
    r"""
    Returns the PlanePartition representation of oi.

    oi is an iterable of 3D coordinates of the form (x, y, z)
    symmeterize specifies whether or not to symmeterize the
    coordinates over the x=y plane.

    """
    tab = oi_to_tab(oi)
    return PlanePartition(tab)


def chain_poset_oi_to_plane_partition(oi):
    r"""
    Takes on order ideal from a product of chains poset as an iterable
    of points of the form ((x,z), y) and returns the PlanePartition formed
    by those points
    """
    i = chain_poset_oi_to_xyz_oi(oi)
    return oi_to_plane_partition(i)


def plane_partition_to_oi(pp):
    r"""
    Returns the order ideal representation of a PlanePartition as a Set
    of coordinates of the form (x, y, z)
    """
    tab = pp.to_tableau()
    #[[2 1],[1]]
    return Set((x,y,z) for x, row in enumerate(tab)
                       for y, zval in enumerate(row)
                       for z in range(0, zval))


def plane_partition_to_chain_poset_oi(pp):
    r"""
    Returns the order ideal representation of a PlanePartition as a
    Set of coordinates of the form ((x,z), y)
    """
    oi = plane_partition_to_oi(pp)
    return xyz_oi_to_chain_poset_oi(oi)


def product_of_3_chains_poset(a,b,c):
    a_chain = Posets.ChainPoset(a)
    b_chain = Posets.ChainPoset(b)
    c_chain = Posets.ChainPoset(c)
    return a_chain.product(b_chain).product(c_chain)


def pp_smallest_box(pp):
    """Find the smallest (x,y,z) box pp could fit inside of"""
    x = len(pp)
    y = len(pp[0])
    z = pp[0][0]
    return (x, y, z)


def box_poset(x, y, z):
    r"""
    Construct a Poset comprised of every point x,y,z inside a box size x,y,z

    Partial order is defined naturally, such that (x,y,z) <= (r,s,t) iff
    x <= r and y <= s and z <= t.
    """

    def leq(thing1, thing2):
        return all(thing1[i] <= thing2[i] for i in range(len(thing1)))

    elems = [(i,j,k) for i in range(x) for j in range(y) for k in range(z)]
    return Poset((elems, leq))


def plot_OI(myposet, OI):
    colors={}
    colors['red']=OI
    p=myposet.plot(vertex_colors=colors,figsize=3.5)
    return p


def save_cspp_rotations(cspp, base_file_name, num_steps=4):
    from PIL import Image

    filenames = ['{0}-{1}.png'.format(base_file_name, i)
                 for i in range(num_steps)]
    angle = -120 / num_steps
    cspp.plot().save(filenames[0])
    orig = Image.open(filenames[0])
    for i in range(num_steps):
        orig.copy().rotate(angle * i) .save(filenames[i])


PlanePartition([[3, 3, 3, 2],
                [3, 3, 3, 2],
                [3, 3, 3, 0],
                [2, 2, 0, 0]]).plot().save('../img/spp.pdf')
PlanePartition([[4, 3, 2, 1],
                [3, 2, 2, 1],
                [2, 1, 0, 0],
                [1, 1, 0, 0]]).plot().save('../img/pp-no-symmetry.pdf')
PlanePartition([[4, 4, 3, 2],
                [4, 4, 3, 2],
                [3, 3, 3, 0],
                [2, 2, 0, 0]]).plot().save('../img/tspp.pdf')
cspp = PlanePartition([[4, 4, 3, 3],
                       [4, 4, 3, 2],
                       [4, 3, 3, 0],
                       [2, 2, 1, 0]])
cspp.plot().save('../img/cspp.pdf')
save_cspp_rotations(cspp, '../img/cspp')

PlanePartition([[3, 1],
                [2, 1]]).plot(show_box=True).save('../img/with-box.pdf')
PlanePartition([[3, 1],
                [2, 1]]).plot().save('../img/without-box.pdf')
tsscpp = PlanePartition([[4, 4, 2, 2],
                         [4, 4, 2, 2],
                         [2, 2, 0, 0],
                         [2, 2, 0, 0]])

tsscpp.plot().save('../img/tsscpp.pdf')
# tsscpp_oi = plane_partition_to_chain_poset_oi(tsscpp)
# plot_OI(product_of_3_chains_poset(2,2,2), tsscpp_oi).save('../img/tsscpp-oi.pdf')

PlanePartition([[2,1],
                [1,0]]).plot().save('../img/test.svg')

p = PlanePartition([[3, 2, 2],
                    [2, 1]])
p.plot().save('../img/transpose-before.pdf')

tab = p.z_tableau()
PlanePartition(map(list, zip(*tab))).plot().save('../img/transpose-after.pdf')
