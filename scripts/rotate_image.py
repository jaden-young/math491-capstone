#!/usr/bin/env python3

import sys
import os
from subprocess import Popen, PIPE
import errno
import svgutils as svg


def px_to_int(px):
    return int(px[:-2])


def convert_svg(filenames, new_format):
    """
    format: 'pdf', 'png'
    """
    fnames = [f.replace('.svg', '') for f in filenames]
    inkscape = Popen(['inkscape', '--shell'], stdin=PIPE)
    for base_name in fnames:
        try:
            inkscape.stdin.write(
                f'{base_name}.svg --export-{new_format}={base_name}.{new_format}\n'.
                encode())
        except IOError as e:
            if e.errno == errno.EPIPE or e.errno == errno.EINVAL:
                break
            else:
                raise

    inkscape.stdin.close()
    inkscape.wait()


def generate_svg_rotations(filename, out_dir, out_format='pdf', num_frames=36):
    tmp = svg.transform.fromfile(filename)
    width = px_to_int(tmp.width) + 50
    height = px_to_int(tmp.height) + 50
    graphic = svg.compose.SVG(filename)
    base_name = os.path.basename(filename).replace('.svg', '')

    angle_delta = 360 / num_frames
    filenames = [
        os.path.join(out_dir, f'{base_name}-{i}.svg')
        for i in range(num_frames)
    ]
    for i, fname in enumerate(filenames):
        if i > 0:
            graphic.rotate(angle_delta, width / 2, height / 2)
        fig = svg.compose.Figure(width, height, graphic)
        fig.save(fname)

    if out_format != 'svg':
        convert_svg(filenames, out_format)
        for f in filenames:
            os.remove(f)


if __name__ == '__main__':
    if len(sys.argv) == 4:
        generate_svg_rotations(sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        generate_svg_rotations(sys.argv[1], sys.argv[2])
