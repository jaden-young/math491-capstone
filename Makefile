SRCNAME=dynamics-of-plane-partitions.tex

.PHONY: dynamics.pdf all clean

all: dynamics.pdf

dynamics.pdf: ${SRCNAME}
	latexmk -bibtex -auxdir=aux -pdf -pdflatex="pdflatex -aux-directory=aux/" -use-make ${SRCNAME}

clean:
	latexmk -CA

figures:
	cd scripts && sage generate-examples.sage

